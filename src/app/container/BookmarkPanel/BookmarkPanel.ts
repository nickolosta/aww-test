import { Component } from '@angular/core';
import {AddBookmarkModalService, BookmarkTreeUIService} from "../../services";

@Component({
  selector: 'bookmark-panel',
  templateUrl: './BookmarkPanel.html',
  styleUrls: ['./BookmarkPanel.scss']
})
export class BookmarkPanel {
  constructor(private AddBookmarkModalService: AddBookmarkModalService, private BookmarkTreeUIService: BookmarkTreeUIService) {

  }
  showAddBookmarkModal = () => {
    this.BookmarkTreeUIService.blur();
    this.AddBookmarkModalService.show(null);
  }
}
