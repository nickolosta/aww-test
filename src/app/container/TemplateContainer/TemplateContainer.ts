import { Component } from '@angular/core';
import {AddBookmarkModalService} from "../../services";

@Component({
  selector: 'template-container',
  templateUrl: './TemplateContainer.html',
  styleUrls: ['./TemplateContainer.scss']
})
export class TemplateContainer {
  subscribes:any = {};
  openedModals = [];
  constructor(private AddBookmarkModalService: AddBookmarkModalService) {
    this.subsribe();
  }
  ngOnInit = () => {

  }
  ngOnDestroy = () => {
    this.unsubsribe();
  }
  subsribe = () => {
    this.subscribes.AddBookmarkModalService = this.AddBookmarkModalService.modal.subscribe((status) => {
      if(status.opened){
        this.openedModals.push("AddBookmarkModal");
      } else {
        this.openedModals = this.openedModals.filter((modal) => {
          return modal != "AddBookmarkModal"
        });
      }
    });
  }
  modalIsOpened = (modalId) => {
    return this.openedModals.includes(modalId);
  }
  unsubsribe = () => {
    for (const subscribe in this.subscribes) {
      this.subscribes[subscribe].unsubsribe();
    }
  }
}
