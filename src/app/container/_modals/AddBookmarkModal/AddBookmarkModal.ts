import { Component } from '@angular/core';
import { uniqueId } from 'lodash';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import {addBookmark, editBookmark} from '../../../store/actions/bookmark.actions';
import { Store } from '@ngrx/store';
import {AddBookmarkModalService} from "../../../services";

@Component({
  selector: 'add-bookmark-modal',
  templateUrl: './AddBookmarkModal.html',
  styleUrls: ['./AddBookmarkModal.scss']
})
export class AddBookmarkModal {
  form: any = null;
  parentID: string = null;
  bookmark:any = null;
  submitted = false;
  subscribes:any = {};
  constructor(private store: Store<{ bookmarks: Array<any> }>, private AddBookmarkModalService: AddBookmarkModalService){
    this.form = new FormGroup({
      name: new FormControl("", [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(100),
      ]),
      page: new FormControl("", [
        Validators.required,
      ]),
    });
    this.subscribe();
  }
  ngOnDestroy = () => {
    this.unsubsribe();
  }
  subscribe = () => {
    this.subscribes.form = this.form.valueChanges.subscribe(val => {
      this.submitted = true;
    });
    this.subscribes.modal = this.AddBookmarkModalService.modal.subscribe(status => {
      this.bookmark = status.bookmark;
      if(status.opened && this.bookmark && !status.parentID){
        this.form.setValue({
          name: this.bookmark.name,
          page: this.bookmark.page,
        });
      } else {
        if(status.parentID){
          this.parentID = status.parentID;
        }
      }
    });
  }
  unsubsribe = () => {
    for (const subscribe in this.subscribes) {
      this.subscribes[subscribe].unsubsribe();
    }
  }
  getTitle = () => {
    return this.bookmark ? "Edit Bookmark" : "Add Bookmark"
  }
  getTitleButton = () => {
    return this.bookmark ? "Edit" : "Add"
  }
  isNumeric = (num) => {
    return !isNaN(num)
  }
  fixNumbers = (event) => {
    const value = `${event.target.value}${event.key}`;
    const isNumber = this.isNumeric(value);
    if(!isNumber && event.keyCode != 8) {
      event.preventDefault();
    }
  }
  onSubmit = () => {
    this.form.updateValueAndValidity();
    if(this.form.status == "VALID"){
      const value = this.form.value;
      // editing mode
      if(this.bookmark){
        let data = {...this.bookmark};
        data.page = parseInt(value.page);
        data.name = value.name;
        this.store.dispatch(editBookmark(data));
      } else {
        let data = {
          id: uniqueId('bookmark_'),
          parentId: (this.parentID ? this.parentID : null),
          page: parseInt(value.page),
          name: value.name,
        };
        this.store.dispatch(addBookmark(data));
      }
      this.hide();
    }
  }
  hide = () => {
    this.AddBookmarkModalService.hide();
  }
}
