import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'left-panel',
  templateUrl: './LeftPanel.html',
  styleUrls: ['./LeftPanel.scss']
})
export class LeftPanel {
  allBookmarks$: Observable<Array<any>>;
  allBookmarks = [];
  subscribes:any = {};
  constructor(private store: Store<{ bookmarks: Array<any> }>) {
    this.allBookmarks$ = store.select('bookmarks');
    this.subsribe();
  }
  subsribe = () => {
    this.subscribes.bookmarks = this.allBookmarks$.subscribe(allBookmarks => {this.allBookmarks = allBookmarks});
  }
  unsubsribe = () => {
    for (const subscribe in this.subscribes) {
      this.subscribes[subscribe].unsubsribe();
    }
  }
  getChildren = () => {
    return this.allBookmarks.filter((item) => {
      return item.parentId == null
    });
  }
  trackByFn = () => {
    return true;
  }
}
