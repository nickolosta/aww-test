import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './App.html',
  styleUrls: ['./App.scss']
})
export class App {
  title = 'Author: Nickolay Stepanov';
}
