import { createReducer, on } from '@ngrx/store';
import { addBookmark, deleteBookmark, editBookmark } from '../actions/bookmark.actions';
 
export const initialState = [
    {
      id: 1,
      parentId: null,
      name: "Bookmark 1",
      page: 1,
    },
    {
      id: 2,
      parentId: null,
      name: "Bookmark 2",
      page: 1,
    },
    {
      id: 3,
      parentId: null,
      name: "Bookmark 3",
      page: 1,
    },
    {
      id: 4,
      parentId: null,
      name: "Bookmark 4",
      page: 1,
    },
    {
      id: 5,
      parentId: 1,
      name: "Bookmark 5",
      page: 1,
    },
    {
      id: 6,
      parentId: 1,
      name: "Bookmark 6",
      page: 1,
    },
    {
      id: 7,
      parentId: 3,
      name: "Bookmark 7",
      page: 1,
    },
];
 
const reducer = createReducer(
  initialState,
  on(addBookmark, (bookmarks, bookmark: any) => {
    let newState = [...bookmarks];
    newState.push(bookmark);
    return newState;
  }),
  on(editBookmark, (bookmarks, bookmark: any) => {
    let newState = [...bookmarks];
    newState = newState.map((_bookmark) => {
      if(_bookmark.id === bookmark.id){
        return bookmark;
      }
      return _bookmark;
    });
    return newState;
  }),
  on(deleteBookmark, (bookmarks, bookmark: any) => {
    let newState = bookmarks.filter((_bookmark) => {
        return _bookmark.id != bookmark.id
    });
    return newState;
  }),
);
 
export function BookmarkReducer(state, action) {
  return reducer(state, action);
}