import { createAction, props } from '@ngrx/store';

export const addBookmark = createAction('[Add Component] Bookmark', props<{ name: string; page: number, id: number, parentId: string | null }>());
export const editBookmark = createAction('[Edit Component] Bookmark', props<{ name: string; page: number, id: number, parentId: string | null }>());
export const deleteBookmark = createAction('[Delete Component] Bookmark', props<{ name: string; page: number, id: number, parentId: string | null }>());