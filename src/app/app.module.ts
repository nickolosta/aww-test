import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { BookmarkReducer } from './store/reducers';
// all container components
import * as containers from './container';
// all UI components
import * as UI from './ui';
//all main services 
import * as services from "./services";

@NgModule({
  declarations: [
    containers.App,
    containers.BookmarkPanel,
    containers.LeftPanel,
    containers.RightPanel,
    containers.TemplateContainer,
    containers.AddBookmarkModal,
    UI.TextInput,
    UI.BookmarkItem,
    UI.FontIconWrapper,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    StoreModule.forRoot({ bookmarks: BookmarkReducer }),
  ],
  providers: [
    services.AddBookmarkModalService,
    services.BookmarkTreeUIService
  ],
  bootstrap: [containers.App]
})
export class AppModule { }
