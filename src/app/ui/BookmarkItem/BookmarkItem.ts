import { Component, Input, HostBinding } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AddBookmarkModalService, BookmarkTreeUIService } from "../../services";
import { deleteBookmark } from "../../store/actions/bookmark.actions";

@Component({
  selector: 'bookmark-item',
  templateUrl: './BookmarkItem.html',
  styleUrls: ['./BookmarkItem.scss']
})
export class BookmarkItem {
  @Input() levelTree: number;
  @Input() item: any;
  @HostBinding('class.bookmark--focused') focused?: boolean;
  allBookmarks$: Observable<Array<any>>;
  allBookmarks = [];
  subscribes:any = {};
  constructor(private store: Store<{ bookmarks: Array<any> }>, private AddBookmarkModalService: AddBookmarkModalService, private BookmarkTreeUIService: BookmarkTreeUIService) {
    this.allBookmarks$ = store.select('bookmarks');
    this.subsribe();
  }
  ngOnInit = () => {

  }
  ngOnDestroy = () => {
    this.unsubsribe();
  }
  subsribe = () => {
    this.subscribes.bookmarks = this.allBookmarks$.subscribe(allBookmarks => {this.allBookmarks = allBookmarks});
    this.subscribes.BookmarkTreeUIService__focused = this.BookmarkTreeUIService.focused.subscribe((bookmarkID) => {
      if(this.item && this.item.id == bookmarkID){
        this.focused = true;
      } else {
        this.focused = false;
      }
    });
  }
  unsubsribe = () => {
    for (const subscribe in this.subscribes) {
      this.subscribes[subscribe].unsubsribe();
    }
  }
  getChildren = () => {
    return this.allBookmarks.filter((item) => {
      return item.parentId == this.item.id
    });
  }
  getStyle = () => {
    let style: any = {'padding-left': `${(this.levelTree * 20) + 10}px`};
    return style;
  }
  getLevelTree = () => {
    return (this.levelTree + 1)
  }
  focus = () => {
    this.BookmarkTreeUIService.focus(this.item);
  }
  isOpened = () => {
    return this.BookmarkTreeUIService.isOpened(this.item);
  }
  hasChildren = () => {
    return this.getChildren().length > 0;
  }
  toggle = (e) => {
    e.stopPropagation();
    this.BookmarkTreeUIService.toggle(this.item);
  }
  addBookmark = () => {
    this.AddBookmarkModalService.show(null, this.item.id);
  }
  editBookmark = () => {
    this.AddBookmarkModalService.show(this.item);
  }
  deleteBookmark = () => {
    this.store.dispatch(deleteBookmark(this.item));
  }
  DnDBookmark = () => {

  }
  trackByFn = () => {
    return true;
  }
}
