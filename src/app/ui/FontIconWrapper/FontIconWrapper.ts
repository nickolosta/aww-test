import { Component, Input } from '@angular/core';
import * as Fonts from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'font-icon',
  templateUrl: './FontIconWrapper.html',
  styleUrls: ['./FontIconWrapper.scss']
})
export class FontIconWrapper {
  @Input() icon: string;
  @Input() size: string;
  @Input() active?: boolean;
  @Input() style?: object;
  @Input() onClick?: Function;
  fonts = Fonts;
  constructor() {

  }
  ngOnInit(){
    
  }
  getIcon = () => {
    return this.fonts[this.icon];
  }
  getSize = () => {
    return this.size;
  }
  getStyle = () => {
    let style: any = {'font-size': this.getSize()};
    if(this.active){
      style.color = "orange";
    }
    if(this.style){
      for (const _style in this.style) {
        style[_style] = this.style[_style];
      }
    }
    return style;
  }
  _onClick = () => {
    if(this.onClick){
      this.onClick();
    }
  }
}
