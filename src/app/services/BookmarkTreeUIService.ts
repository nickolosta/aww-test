import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BookmarkTreeUIService {
  opened:BehaviorSubject<Array<any>> = new BehaviorSubject([]);
  focused:BehaviorSubject<string> = new BehaviorSubject(null);  

  constructor(){}
  focus = (bookmark) => {
    this.focused.next(bookmark.id);
  }
  blur = () => {
    this.focused.next(null);
  }
  isFocused = (bookmark) => {
    return this.focused.getValue() == bookmark.id;
  }
  isOpened = (bookmark) => {
    if(bookmark){
        return this.opened.getValue().includes(bookmark.id);
    }
    return false;
  }
  toggle = (bookmark: any) => {
    if(this.opened.getValue().includes(bookmark.id)){
        this.hide(bookmark);
    } else {
        this.show(bookmark);
    }
  }
  show = (bookmark: any) => {
    let openedClone =  [...this.opened.getValue()];
    openedClone.push(bookmark.id);
    this.opened.next(openedClone);
  }
  hide = (bookmark: any) => {
    let openedClone =  [...this.opened.getValue()];
    openedClone = openedClone.filter((_bookmark) => {
        return _bookmark != bookmark.id
    });
    this.opened.next(openedClone);
  }
}