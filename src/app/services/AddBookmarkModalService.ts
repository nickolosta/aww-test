import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AddBookmarkModalService {
  modal:BehaviorSubject<any> = new BehaviorSubject({opened: false, bookmark: null});  
  constructor(){}
  show = (bookmark: any, parentID?: string) => {
    this.modal.next({opened: true, bookmark, parentID});
  }
  hide = () => {
    this.modal.next({opened: false, bookmark: null, parentID: null});
  }
}